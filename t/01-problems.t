#!perl

use Test::More;

use Acme::ProjectEuler qw(problem1 problem2);      # import listed symbols

BEGIN {
    use_ok( 'Acme::ProjectEuler' ) || print "Bail out!\n";
}

is( problem1(), 233168, "Checking Problem 1");
is( problem2(0), 0, "Checking fib(0)");
is( problem2(1), 1, "Checking fib(1)");
is( problem2(2), 1, "Checking fib(2)");
is( problem2(3), 2, "Checking fib(3)");
is( problem2(4), 3, "Checking fib(4)");
is( problem2(5), 5, "Checking fib(5)");
is( problem2(12), 144, "Checking fib(5)");

done_testing();