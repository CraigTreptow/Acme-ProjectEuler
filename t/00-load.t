#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Acme::ProjectEuler' ) || print "Bail out!\n";
}

diag( "Testing Acme::ProjectEuler $Acme::ProjectEuler::VERSION, Perl $], $^X" );
